"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var APIService = /** @class */ (function () {
    function APIService(_authToken) {
        this._authToken = _authToken;
        this._method = "GET";
        this._headers = [];
    }
    Object.defineProperty(APIService.prototype, "authToken", {
        get: function () {
            return this._authToken;
        },
        set: function (newAuthToken) {
            this._authToken = newAuthToken;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIService.prototype, "headers", {
        get: function () {
            return this._headers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIService.prototype, "method", {
        set: function (newMethod) {
            this._method = newMethod;
        },
        enumerable: true,
        configurable: true
    });
    APIService.prototype.setHeaders = function (headers) {
        for (var i in headers) {
            if (headers[i].hasOwnProperty('key')
                && headers[i].hasOwnProperty('value')) {
                this._headers.push([headers[i].key, headers[i].value]);
            }
        }
        return this;
    };
    APIService.prototype.resetHeaders = function () {
        this._headers = [];
    };
    APIService.prototype.setMethod = function (newMethod) {
        this._method = newMethod;
        return this;
    };
    APIService.prototype.request = function (body) {
        return {
            headers: this._headers,
            method: this._method,
            body: JSON.stringify(body)
        };
    };
    return APIService;
}());
exports.APIService = APIService;
var RequestBody = /** @class */ (function () {
    function RequestBody(_requestBody) {
        this._requestBody = _requestBody;
    }
    Object.defineProperty(RequestBody.prototype, "requestBody", {
        get: function () {
            return this._requestBody;
        },
        set: function (newRequestBody) {
            this._requestBody = newRequestBody;
        },
        enumerable: true,
        configurable: true
    });
    return RequestBody;
}());
exports.RequestBody = RequestBody;
exports.default = APIService;
