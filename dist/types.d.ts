export declare type KeyValue<T, U> = {
    key: T;
    value: U;
};
export declare type APIMethod = "POST" | "GET" | "PUT" | "DELETE";
