import { KeyValue, APIMethod } from "./types";
export declare class APIService {
    private _authToken;
    private _method;
    private _headers;
    constructor(_authToken: string);
    authToken: string;
    readonly headers: string[][];
    method: APIMethod;
    setHeaders(headers: KeyValue<string, string>[]): APIService;
    resetHeaders(): void;
    setMethod(newMethod: APIMethod): APIService;
    request<T>(body: T): RequestInit;
}
export declare class RequestBody<T> {
    private _requestBody;
    constructor(_requestBody: T);
    requestBody: T;
}
export default APIService;
