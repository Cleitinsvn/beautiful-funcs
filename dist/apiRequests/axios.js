"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var APIService = /** @class */ (function () {
    function APIService(_authToken) {
        this._authToken = _authToken;
    }
    Object.defineProperty(APIService.prototype, "authToken", {
        get: function () {
            return this._authToken;
        },
        set: function (newAuthToken) {
            this._authToken = newAuthToken;
        },
        enumerable: true,
        configurable: true
    });
    return APIService;
}());
exports.APIService = APIService;
