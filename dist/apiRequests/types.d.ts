export declare type KeyValue<T, U> = {
    key: T;
    value: U;
};
