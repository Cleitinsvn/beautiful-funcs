export declare class APIService {
    private _authToken;
    constructor(_authToken: string);
    authToken: string;
}
export declare type ApiHeader = {
    key: string;
    value: string;
};
