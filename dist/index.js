"use strict";
function personFactory(name) {
    var count = 0;
    return {
        opa: function () { return count++; },
        name: name,
        type: 'person'
    };
}
var cleiton = personFactory('Cleiton');
var joao = personFactory('João');
console.log(cleiton.opa());
console.log(joao.opa());
var point;
var otherPoint = { x: 0, y: 0, z: 0 };
point = otherPoint;
console.log(point);
console.log(otherPoint);
point = { x: 1, y: 2, z: 0 };
console.log(point);
