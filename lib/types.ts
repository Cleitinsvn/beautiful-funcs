export type KeyValue<T, U> = {
	key: T,
	value: U
}

export type APIMethod = "POST" | "GET" | "PUT" | "DELETE";